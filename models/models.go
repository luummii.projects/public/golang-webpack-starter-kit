package models

import (
	"time"
)

// User model.
type User struct {
	ID       int64  `json:"id,omitempty"`
	Username string `json:"username"`
}

// RegisterUserInput model.
type RegisterUserInput struct {
	Email, Username string
}

// LoginInput model.
type LoginInput struct {
	Email string
}

// LoginOutput model.
type LoginOutput struct {
	Token     string    `json:"token"`
	ExpiresAt time.Time `json:"expiresAt"`
	AuthUser  User      `json:"authUser"`
}

// IndexData model for template html.
type IndexData struct {
	Data string
}
