package services

import (
	"errors"
	"time"
)

var (
	// ErrUserNotFound denotes that the user was not found.
	ErrUserNotFound = errors.New("user not found")
	// ErrInvalidEmail denotes a mal formated email address.
	ErrInvalidEmail = errors.New("invalid email")
	// ErrInvalidUsername denotes an username not matching the proper format.
	ErrInvalidUsername = errors.New("invalid username")
	// ErrUsernameTaken denotes there is a user with that name already.
	ErrUsernameTaken = errors.New("username taken")
	// ErrEmailTaken denotes there is a user with that email already.
	ErrEmailTaken = errors.New("email taken")
	// ErrUnauthenticated denotes no authenticated user in context.
	ErrUnauthenticated = errors.New("unauthenticated")
)

const (
	// TokenLifespan until tokens are valid.
	TokenLifespan = time.Hour * 24 * 14
	// KeyAuthUserID to use in context.
	KeyAuthUserID key = "auth_user_id"
)

type key string
