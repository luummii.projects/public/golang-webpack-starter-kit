package services

import (
	"database/sql"

	"github.com/hako/branca"
)

// Service contains the core business logic separated from the transport layer.
// You can use it to back a REST API.
type Service struct {
	db    *sql.DB
	codec *branca.Branca
}

// New service implementation.
func New(db *sql.DB, codec *branca.Branca) *Service {
	return &Service{
		db:    db,
		codec: codec,
	}
}
