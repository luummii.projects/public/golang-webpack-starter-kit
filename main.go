package main

import (
	"database/sql"
	"flag"
	"log"
	"net/http"
	"os"
	"projects/golang-webpack-starter-kit/handlers"
	"projects/golang-webpack-starter-kit/services"

	"github.com/gobuffalo/packr"
	"github.com/hako/branca"
	"github.com/joho/godotenv"
	migrate "github.com/rubenv/sql-migrate"

	// this is postgres drivers
	_ "github.com/jackc/pgx/stdlib"
)

var (
	localHost string
	port      string
	db        *sql.DB
	codec     *branca.Branca
)

func init() {
	// Init ENV variables
	if err := godotenv.Load("config.env"); err != nil {
		panic(err)
	}
	if p := os.Getenv("PORT"); len(p) > 0 {
		port = p
	}
	if lh := os.Getenv("LOCALHOST"); len(lh) > 0 {
		localHost = lh
	}
}

func main() {
	// Connect database
	dsn := os.Getenv("DSN")
	// for start on heroku
	if os.Getenv("DATABASE_URL") != "" {
		dsn = os.Getenv("DATABASE_URL")
	}

	db, err := sql.Open("pgx", dsn)
	defer db.Close()
	if err != nil {
		panic("ERROR CONNECT TO DATABASE! CHECK PARAMS FOR CONNECT (url, password and more...)")
	}
	err = db.Ping()
	if err != nil {
		log.Printf("ERROR DB PING:%s\n", err)
		panic(err)
	}

	// For migration
	isMigrations := flag.Bool("migrate", false, "migrations mode")
	flag.Parse()
	if *isMigrations {
		migrations := &migrate.PackrMigrationSource{
			Box: packr.NewBox("./migrations"),
		}
		n, err := migrate.Exec(db, "postgres", migrations, migrate.Up)
		if err != nil {
			log.Printf("Mirgation is error: %s", err)
		}
		log.Printf("Applied %d migrations!\n", n)
		return
	}

	// Branca is a secure alternative to JWT
	codec = branca.NewBranca(os.Getenv("BRANCA"))
	codec.SetTTL(uint32(services.TokenLifespan.Seconds()))

	s := services.New(db, codec)
	h := handlers.New(s)

	log.Printf("Accepting connections on port: %s\n", port)
	if err := http.ListenAndServe(":"+port, h); err != nil {
		log.Fatalf("ERROR: Could not start server: %v\n", err)
	}
}
