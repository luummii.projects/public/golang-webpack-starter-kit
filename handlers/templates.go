package handlers

import (
	"html/template"
	"net/http"
	"projects/golang-webpack-starter-kit/models"
)

func (h *handler) index(w http.ResponseWriter, r *http.Request) {
	d := models.IndexData{Data: r.URL.Path}
	template, _ := template.ParseFiles("templates/index.html")
	template.Execute(w, d)
}
