package handlers

import (
	"net/http"
	"projects/golang-webpack-starter-kit/services"

	"github.com/matryer/way"
)

type handler struct {
	*services.Service
}

// New makes use of the service to provide an http.Handler with predefined routing.
func New(s *services.Service) http.Handler {
	h := &handler{s}

	templ := way.NewRouter()
	templ.HandleFunc("GET", "/", h.index)

	api := way.NewRouter()
	api.HandleFunc("POST", "/login", h.login)
	api.HandleFunc("POST", "/register", h.register)

	auth := way.NewRouter()
	auth.HandleFunc("GET", "/auth_user", h.authUser)

	r := way.NewRouter()
	r.Handle("*", "/api...", http.StripPrefix("/api", api))
	r.Handle("*", "/auth...", http.StripPrefix("/auth", h.withAuth(auth)))
	r.Handle("*", "/", templ)

	return r
}
