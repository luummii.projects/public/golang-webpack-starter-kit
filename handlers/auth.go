package handlers

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"projects/golang-webpack-starter-kit/models"
	"projects/golang-webpack-starter-kit/services"
	"strings"
)

func (h *handler) login(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var in models.LoginInput
	if err := json.NewDecoder(r.Body).Decode(&in); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	out, err := h.Login(r.Context(), in.Email)

	if err != nil {
		log.Printf("ERROR:login: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	respond(w, out, http.StatusOK)
}

func (h *handler) register(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var in models.RegisterUserInput

	if err := json.NewDecoder(r.Body).Decode(&in); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err := h.Register(r.Context(), in.Email, in.Username)
	if err == services.ErrInvalidEmail || err == services.ErrInvalidUsername {
		http.Error(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}

	if err == services.ErrEmailTaken || err == services.ErrUsernameTaken {
		http.Error(w, err.Error(), http.StatusConflict)
		return
	}

	if err != nil {
		log.Printf("ERROR:register: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (h *handler) withAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		a := r.Header.Get("Authorization")
		if !strings.HasPrefix(a, "Bearer ") {
			// next.ServeHTTP(w, r)
			http.Error(w, "user unauthorized", http.StatusUnauthorized)
			return
		}

		token := a[7:]
		uid, err := h.AuthUserID(token)
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, services.KeyAuthUserID, uid)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (h *handler) authUser(w http.ResponseWriter, r *http.Request) {
	u, err := h.AuthUser(r.Context())
	if err == services.ErrUnauthenticated {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if err == services.ErrUserNotFound {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		log.Printf("ERROR:authUser: %s", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	respond(w, u, http.StatusOK)
}
